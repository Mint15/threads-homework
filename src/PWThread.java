public class PWThread extends Thread{

    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    @Override
    public void run() {
        int timeSleep = getRandomNumber(100, 1000);
        try {
            Thread.sleep(timeSleep);
            System.out.println(this.getName() + " запустился за " + timeSleep + " мс");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
