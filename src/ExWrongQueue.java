public class ExWrongQueue {

    public static void main(String[] args) {
        new ExWrongQueue().go();
    }

    private void go() {

        MyQ myQ = new MyQ();

        new Producer(myQ);
        new Consumer(myQ);

    }


    class Producer implements Runnable {

        public MyQ queue;

        public Producer(MyQ queue) {
            this.queue = queue;
            new Thread(this, "Поставщик").start();
        }

        @Override
        public void run() {
            int i = 0;

            while (true) {
                queue.put(i++);
            }
        }
    }

    class Consumer implements Runnable {

        public MyQ queue;

        public Consumer(MyQ queue) {
            this.queue = queue;
            new Thread(this, "Потребитель").start();
        }

        @Override
        public void run() {
            while (true) {
                queue.get();
            }
        }
    }

    class MyQ {
        int n;

        synchronized void put(int n) {
            try {
                Thread.sleep(100);
                this.n = n;
                System.out.println("Отправили: " + n);
                notify();
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        synchronized int get() {
            try {
                Thread.sleep(100);
                System.out.println("Получили: " + n);
                notify();
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return n;

        }
    }
}

