public class TicTac {

    public static void main(String[] args) {
        new TicTac().go();
    }

    private void go() {

        MyQ myQ = new MyQ();

        new Tic(myQ);
        new Tac(myQ);

    }


    class Tic implements Runnable {

        public MyQ queue;

        public Tic(MyQ queue) {
            this.queue = queue;
            new Thread(this, "Тик").start();
        }

        @Override
        public void run() {
            int i = 0;

            while (true) {
                queue.tic();
            }
        }
    }

    class Tac implements Runnable {

        public MyQ queue;

        public Tac(MyQ queue) {
            this.queue = queue;
            new Thread(this, "Так").start();
        }

        @Override
        public void run() {
            while (true) {
                queue.tac();
            }
        }
    }

    class MyQ {
        int n;

        synchronized void tic(){
            try {
                Thread.sleep(100);
                System.out.println("Тик");
                notify();
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        synchronized void tac() {
            try {
                Thread.sleep(100);
                System.out.println("Так");
                notify();
                wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}

