import java.util.Random;

public class Runner extends Thread{

    @Override
    public void run() {
        Random random = new Random();
        for (int i = 0; i < 5; i++) {
            int time = random.nextInt(2500);
            try {
                Thread.sleep(time);
                System.out.println(i + 1 + " точку бегун пробежал за " + time + " мс");
            } catch (InterruptedException e) {
                break;
            }
        }
    }
}
