public class Timer {

    static final int MAX_SLEEP_TIME = 5000;

    public static void main(String[] args) {
        Runner runner = new Runner();
        runner.start();
        try {
            Thread.sleep(MAX_SLEEP_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (runner.isAlive()){
            runner.interrupt();
            System.out.println("Бегун не справился с задачей");
        } else {
            System.out.println("Финиш!");
        }
    }
}
